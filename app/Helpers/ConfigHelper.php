<?php


use App\GlobalModel;

if (! function_exists('get_config')) {

    /**
     * Append an ordinal indicator to a numeric value.
     *
     * @param  string  $name
     * @param  string  $column
     * @return string
     */
    function get_config($name ,$column ='value'){
        $setting = \App\SiteSetting::where('name' , $name)->first();
        return $setting->{$column};
    }

}
if (! function_exists('get_global_model')) {

    /**
     * Append an ordinal indicator to a numeric value.
     *
     * @param  string  $name
     * @return string
     */
    function get_global_model($name){
        $setting = GlobalModel::where('name' , $name)->first();
        return $setting->status;
    }

}
