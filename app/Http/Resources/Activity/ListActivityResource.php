<?php

namespace App\Http\Resources\Activity;


use App\Http\Resources\Package\PackageHotelResource;
use App\Http\Resources\Package\PackageLabelResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class ListActivityResource extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function ($data) {
                if ($data->image != '') {
                    $image = url('storage/app/public/images/activity', $data->image);
                } else {
                    $image = '';
                }
                return [
                    'id' => $data->id,
                    'name' => $data->name,
                    'intro' => $data->intro,
                    'overview' => $data->overview,
                    'info' => $data->info,
                    'term' => $data->term,
                    'deadline' => $data->deadline,
                    'symbol' => $data->symbol,
                    'image' => $image,
                    'activity_country_id' => $data->activity_country_id,
                    'activity_city_id' => $data->activity_city_id,
                    'activity_duration' => $data->activity_duration,
                    'activity_service' => $data->activity_service,
                    'activity_for' => $data->activity_for,
                    'price' => $data->price,
                    'date' => $data->date,
                    'cancellation_date' => $data->cancellation_date,
                    'private' => $data->private,
                    'shared' => $data->shared,

                ];

            })
        ];
    }
    public function with($request)
    {
        return [
            "success" => true,
            "message" => "",
            "status" => 200
        ];
    }
}
