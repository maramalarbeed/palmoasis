<?php


namespace App\Http\Controllers\Backend;


use App\ActivityTransaction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TransactionsController extends Controller
{

    public function activities(Request $request)
    {
        $transactions = ActivityTransaction::all();
        return view('backend.transactions.index_activities', compact('transactions'));
    }
}
