<?php


namespace App\Http\Controllers\Backend;


use App\ActivityOrder;
use App\Enquiry;
use App\Http\Controllers\Controller;
use App\VisaUaeApplication;

class TransactionController extends Controller
{
    public function index_enquiry($id)
    {
        $enquiry = Enquiry::findOrFail($id);
        return view('backend.transactions.index_enquiry', compact('enquiry'));
    }

    public function index_activity($id)
    {
        $order = ActivityOrder::findOrFail($id);
        return view('backend.transactions.index_activities', compact('order'));
    }

    public function index_uae_visa($id)
    {
        $application = VisaUaeApplication::findOrFail($id);
        return view('backend.transactions.index_uae_visa', compact('application'));
    }
}
