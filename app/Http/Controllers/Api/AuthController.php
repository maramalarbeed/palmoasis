<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Member;
use App\User;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $loginData = $request->all();
        $validatedData = Validator::make($loginData, [
            'email' => 'email|required',
            'password' => 'required'
        ]);
        if ($validatedData->fails()) {
            $data = [
                'success' => false,
                "message" => trans('exception.Validation-Error'),
                'data' => $validatedData->errors(),
                "count" => count($validatedData->errors()),
                "status" => 422
            ];
            throw new HttpResponseException(response()->json(
                $data, 422));
        }
        return $this->proxy('password', [
            'username' => $loginData['email'],
            'password' => $loginData['password']
        ]);
    }

    public function register(Request $request)
    {
        $validatedData = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:55'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:members'],
            'password' => ['required', 'string', 'min:8'],
        ]);

        if ($validatedData->fails()) {
            $data = [
                'success' => false,
                "message" => trans('exception.Validation-Error'),
                'data' => $validatedData->errors(),
                "count" => count($validatedData->errors()),
                "status" => 422
            ];
            throw new HttpResponseException(response()->json(
                $data, 422));
        }
        $password = $request->password;

        $member = new Member();
        $member->email = $request->email;
        $member->phone = $request->phone;
        $member->name = $request->name;
        $member->password = bcrypt($password);

        // Attempt to log the user in
        if ($member->save()) {
            return $this->proxy('password', [
                'username' => $request->input('email'),
                'password' => $password
            ]);
        } else {
            return response()->json([
                "success" => false,
                "message" => "",
                "data" => "",
                "total" => 1,
                "status" => 500
            ]);
        }
    }

    public function proxy($grant_type, $data)
    {
        $data = array_merge($data, [
            'client_id' => config('app.PASSWORD_CLIENT_ID'),
            'client_secret' => config('app.PASSWORD_CLIENT_SECRET'),
            'grant_type' => $grant_type,
            'scope' => '*',
        ]);

        $url = url('/oauth/token');

        $response = Http::withOptions([
            'curl' => [
//                CURLOPT_DNS_USE_GLOBAL_CACHE => true,
//                CURLOPT_DNS_CACHE_TIMEOUT => 2,
            ]
        ])->acceptJson()->post($url, $data);

        $result = json_decode((string)$response->getBody(), true);
        return response()->json($result, $response->getStatusCode());
    }
}
