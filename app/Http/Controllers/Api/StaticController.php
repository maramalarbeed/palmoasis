<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\SiteSetting;
use Illuminate\Support\Facades\Config;

class StaticController extends Controller
{
    public function general_info()
    {
        $IATA_Logo = SiteSetting::where('name', 'IATA_Logo')->first();

        return response()->json([
            "success" => true,
            "message" => "",
            "data" => [
                'site_name' => get_config('site_name'),
                'phone' => get_config('office'),
                'email' => get_config('email'),
                'facebook' => get_config('facebook'),
                'twitter' => get_config('twitter'),
                'youtube' => get_config('youtube'),
                'instagram' => get_config('instagram'),
                'main_office' => get_config('main_office'),
                'whatsapp' => get_config('whatsapp'),
                'apple_store' => get_config('apple'),
                'google_play' => get_config('google'),
                'active_activity' => intval(get_global_model('activity')),
                'active_outbound_isa' => intval(get_global_model('outboundVisa')),
                'active_use_visa' => intval(get_global_model('uaeVisa')),
                'intro_title' => get_config('intro' ,'title'),
                'intro_value' => get_config('intro'),
                'master_card_img' => asset('img/master-card.png'),
                'visa_img' => asset('img/visa.png'),
                'iata_logo' => ($IATA_Logo->value != '') ? url('storage/app/public/images/settings/' . $IATA_Logo->value) : "",
                'messenger_id' => '105961591242843',
                'whit_logo' => asset('img/logo-white.png'),
                'logo' => asset('img/logo.png')

            ],
            "total" => 1,
            "status" => 200
        ]);
    }
}
