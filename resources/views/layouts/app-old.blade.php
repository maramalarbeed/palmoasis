<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="csrf-token-frontend" content="{{ csrf_token() }}">

    <title>{{ Config::get('site_settings.site_name') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    {{--    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"--}}
    {{--          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
          integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <link href="{{ asset('frontend/css/normalize.css') }}" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('frontend/'. app()->getLocale() .'/bootstrap/css/bootstrap.css') }}"
          rel="stylesheet">
    <link href="{{ asset('frontend/css/normalize.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/fontello.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/fonts/icon-7-stroke/css/pe-icon-7-stroke.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/fonts/icon-7-stroke/css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/bootstrap-select.min.css') }}" rel="stylesheet">

    <link href="{{ asset('frontend/css/icheck.min_all.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/price-range.css') }}" rel="stylesheet">

    <link href="{{ asset('frontend/css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/owl.theme.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/owl.transitions.css') }}" rel="stylesheet">

    <link href="{{ asset('frontend/css/lightslider.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/style_chat_box.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/bootstrap-datepaginator.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/bootstrap-datepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/slick.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/slick-theme.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/swiper.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/animate.min.css') }}" rel="stylesheet">

    @if (Config::get('site_settings.font') == '2')
        <link href="https://fonts.googleapis.com/css2?family=Cairo&display=swap" rel="stylesheet">
        <style>
            body {
                font-family: "Cairo" !important;
            }
        </style>
    @elseif (Config::get('site_settings.font') == '3')
        <link href="//db.onlinewebfonts.com/c/02f502e5eefeb353e5f83fc5045348dc?family=GE+SS+Two+Light" rel="stylesheet"
              type="text/css"/>
        <style>
            body {
                font-family: 'GE SS Two Light' !important;
            }
        </style>
    @else
        <link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/droid-arabic-kufi" type="text/css"/>
        <style>
            body {
                font-family: 'DroidArabicKufiRegular' !important;
            }
        </style>
    @endif


    <link href="{{ asset('frontend/css/default-theme.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/stylehome.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/style_'.app()->getLocale() .'.css') }}" rel="stylesheet">

</head>
<body>
<input type="hidden" id='lang_session' value="{{app()->getLocale()}}"/>

<div class="header-connect">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-8  col-xs-12">
                <div class="header-half header-call">
                    <p style="color:white;">
                        @if(Config::get('site_settings.office') != '')
                            <span><i class="pe-7s-call"></i> <b>
                                            <a href="tel:{{Config::get('site_settings.office')}} " style="color:#fff">
                                             {{Config::get('site_settings.office')}}
                                            </a></b></span>
                        @endif
                        @if(Config::get('site_settings.email') != '')
                            <span><i class="pe-7s-mail"></i> <b>
                                            <a href="mailto:{{Config::get('site_settings.email')}}" style="color:#fff">
                                         {{Config::get('site_settings.email')}}
                                            </a></b></span>
                        @endif
                    </p>
                </div>
            </div>
            <div class="col-md-3 col-sm-1"></div>
            <div class="col-md-4 col-sm-3 col-xs-12 navbar-ot-text-right">
                <div class="header-half header-social">
                    <ul class="list-inline">
                        @if(Config::get('site_settings.facebook') != '')
                            <li>
                                <a href="{{Config::get('site_settings.facebook')}}" target="_blank">
                                    <i class="fab fa-facebook"></i>
                                </a>
                            </li>
                        @endif
                        @if(Config::get('site_settings.twitter') != '')
                            <li>
                                <a href="{{Config::get('site_settings.twitter')}}" target="_blank">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                        @endif
                        @if(Config::get('site_settings.youtube') != '')
                            <li>
                                <a href="{{Config::get('site_settings.youtube')}}" target="_blank">
                                    <i class="fab fa-youtube"></i>
                                </a>
                            </li>
                        @endif
                        @if(Config::get('site_settings.instagram') != '')
                            <li>
                                <a href="{{Config::get('site_settings.instagram')}}" target="_blank">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                        @endif
                        <li>
                            @if (app()->getLocale() != 'en')
                                <a href="{{route('setlocale','en')}}">
                                    {{trans('messages.English')}}
                                </a>
                            @else
                                <a href="{{route('setlocale','ar')}}">
                                    {{trans('messages.Arabic')}}
                                </a>
                            @endif
                        </li>

                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>
<!--End top header -->

<nav class="navbar navbar-default ">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand navbar-brand-custom" href="{{route('home')}}"><img
                    src="{{asset('img/logo.png')}}" alt=""></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse yamm" id="navigation">
            <div class="button navbar-ot-right" id="member-zone-div" style="padding-top: 17px;">

                @if(!Auth::guard('member')->check())
                    <a class="navbar-btn nav-button wow bounceInRight login"
                       href="{{route('member.login')}}"
                       data-wow-delay="0.45s">
                        {{trans('messages.Login')}}
                    </a>

                    <button class="navbar-btn nav-button nav-button wow fadeInRight register"
                            onclick="openEnquiryForm()" data-wow-delay="0.48s">
                        {{trans('messages.Enquiry')}}
                    </button>
                @else
                    <span class="wow bounceInRight span-welcome" data-wow-delay="0.45s">
                                {{trans('messages.welcome2')}} {{\Illuminate\Support\Facades\Auth::guard('member')->user()->name}}
                            </span>
                    <a class="navbar-btn nav-button wow bounceInRight login" href="{{route('member.logout')}}"
                       data-wow-delay="0.45s" title=" {{trans('messages.Logout')}}">
                        <i class="fab fa-sign-out"></i>
                    </a>
                    <a class="navbar-btn nav-button wow bounceInRight login" href="{{route('member.account')}}"
                       data-wow-delay="0.45s" title=" {{trans('messages.My account')}}">
                        <i class="fa fa-user"></i>
                    </a>
                    @if (Config::get('global_models.activity') == '1')
                        <div title="{{trans('messages.My Cart')}}"
                             class="nav-button bounceInRight login activity-cart-container"
                             data-wow-delay="0.45s" style="display:inline-block;padding: 3px 20px">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false" id="activity-card">
                                <i class="fa fa-shopping-cart" style="color: #d27e04;font-size: 15px"></i>
                                <ul class="dropdown-menu notify-drop" aria-labelledby="activity-card">
                                    @if(Auth::guard('member')->user()->orderActivity)
                                        <div class="notify-drop-title">
                                            <div class="row">
                                                <div class="col-xs-12 text-center">
                                                    <b>{{trans('messages.Cart')}}
                                                        : {{Auth::guard('member')->user()->orderActivity->card->count()}} {{trans('messages.Item')}}</b>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end notify title -->
                                        <!-- notify content -->
                                        <div class="drop-content">
                                            @foreach(Auth::guard('member')->user()->orderActivity->card as $cart_activity)
                                                <li>
                                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                                        <div class="cart-drop-img img-responsive">
                                                            <img
                                                                src="{{url('storage/app/public/images/activity/'.$cart_activity->activity->image)}}"
                                                                alt=""/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-7 col-sm-7 col-xs-7 pd-l0">
                                                        <h4 style="margin:10px 0;color:#000">
                                                            <b>{{$cart_activity->price}} {{trans('messages.this_currency')}}</b>
                                                        </h4>
                                                        <h4 style="margin-bottom: 10px;margin-top: 0;color:#005da1">{{$cart_activity->activity->name}}</h4>
                                                        <p>{{$cart_activity->date}}</p>
                                                        <p>{{$cart_activity->adult}} {{trans('messages.Adults')}}
                                                            , {{$cart_activity->child}} {{trans('messages.Children')}}</p>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </div>
                                        <div class="drop-footer">
                                            <div class="row text-center align-middle total-price">
                                                <div class=" col-md-6 sol-xs-6">
                                                    {{trans('messages.total-price')}}
                                                </div>
                                                <div class="col-md-6 sol-xs-6">
                                                    <h4 style="margin: 0">{{Auth::guard('member')->user()->orderActivity->card->sum('price')}} {{trans('messages.this_currency')}}</h4>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class=" col-md-6 sol-xs-6 text-center">
                                                    <a class="btn btn-blue" href="{{route('activity.card')}}">
                                                        {{trans('messages.View')}}
                                                    </a>
                                                </div>
                                                <div class=" col-md-6 sol-xs-6 text-center">
                                                    <a href="#" class="btn btn-white">
                                                        {{trans('messages.Checkout')}}
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div class="notify-drop-title">
                                            <div class="row">
                                                <div class="col-xs-12 text-center">
                                                    <b>{{trans('messages.Cart')}}: 0 {{trans('messages.Item')}}</b>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end notify title -->
                                        <!-- notify content -->
                                        <div class="drop-content">
                                            <li>
                                                <div class="col-md-12 text-center">
                                                    no item
                                                </div>
                                            </li>
                                        </div>
                                    @endif
                                </ul>

                            </a>
                        </div>
                    @endif
                    <button class="navbar-btn nav-button wow login" data-wow-delay="0.45s"
                            title="{{trans('messages.My Favorites')}}" onclick="openFavoriteModal()">
                        <img src="{{ asset('img/bookmark_add.png') }}" style="width:15px;"/>
                    </button>
                @endguest
                {{--                @if (Config::get('site_settings.apple') != '')--}}
                {{--                <a href="{{Config::get('site_settings.apple')}}" target="_blank">--}}
                {{--                    <img src="{{ asset('img/apple.png') }}" class="navbar-btn wow fadeInRight" data-wow-delay="0.48s">--}}
                {{--                </a>--}}
                {{--                @endif--}}
                {{--                @if (Config::get('site_settings.google') != '')--}}
                {{--                    <a href="{{Config::get('site_settings.google')}}" target="_blank">--}}
                {{--                        <img src="{{ asset('img/google-play.png') }}" class="navbar-btn wow fadeInRight"--}}
                {{--                             data-wow-delay="0.48s">--}}
                {{--                    </a>--}}
                {{--                @endif--}}
            </div>
            <ul class="main-nav nav navbar-nav navbar-ot-left">
                <li class="ymm-sw " data-wow-delay="0.1s">
                    <a href="{{route('/')}}" class="">
                        {{trans('messages.Home')}}
                    </a>
                </li>
                <li class="wow fadeInDown" data-wow-delay="0.3s">
                    <a class="" href="{{route('packages.countries')}}">
                        {{trans('messages.Packages-home-menu')}}
                    </a>
                </li>
                @if (Config::get('global_models.activity') == '1')
                    <li class="wow fadeInDown" data-wow-delay="0.4s">
                        <a class="" href="{{route('activity.index')}}">
                            {{trans('messages.Activities')}}
                        </a>

                    </li>
                @endif
                <li class="wow fadeInDown" data-wow-delay="0.3s">
                    <a class="<?php if (isset($blog_active) && $blog_active) { ?> active <?php } ?>"
                       href="{{route('blogs')}}">
                        {{trans('messages.Blog')}}
                    </a>
                </li>
                @if (Config::get('global_models.uaeVisa') == '1' || Config::get('global_models.outboundVisa') == '1')
                    <li class="dropdown wow fadeInDown" data-wow-delay="0.3s">
                        <a type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false" href="#">
                            {{trans('messages.Visa')}}
                            <i class="fas fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            @if (Config::get('global_models.uaeVisa') == '1')
                                <li>
                                    <a class="dropdown-item" href="{{route('visa.uae')}}">
                                        {{trans('messages.Uae Visa')}}
                                    </a>
                                </li>
                            @endif
                            @if(Config::get('global_models.outboundVisa') == '1')
                                <li>
                                    <a class="dropdown-item" href="{{route('visa.outbound')}}">
                                        {{trans('messages.Outbound Visa')}}
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </li>
                @endif
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
@include('sweetalert::alert')
@yield('content')
<div id="back-to-top-button">
    <a onclick="topFunction()" title="">
        <i class="fas fa-arrow-up"></i>
    </a>
</div>
<style>
    .we_accept {
        margin-top: -35px;
    }
</style>
@php
    $IATA_Logo = \App\SiteSetting::where('name', 'IATA_Logo')->first();
@endphp
<div class="footer-area">
    <div class=" footer">
        <div class="container">
            <div class="row footer-row">
                <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>
                            {{trans('messages.Safe and secure')}}
                        </h4>
                        <div class="footer-title-line"></div>
                        <p>
                            <b>
                        <div class="we_accept">
                            {{trans('messages.We-Accept')}}
                        </div>
                        </b>
                        </p>
                        <div class="col-md-12 p-0" style="font-size: 11px;">
                            <img src="{{ asset('img/master-card.png') }}"
                                 style="    width: 44px;" alt="" class="wow pulse" data-wow-delay="1s">

                            <img src="{{ asset('img/visa.png') }}" alt=""
                                 style="    width: 44px;" class="wow pulse" data-wow-delay="1s"
                                 style="border-radius: 0px;">
                            {{trans('messages.Pay 1')}}
                        </div>

                    </div>
                </div>
                <div class="col-md-2 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>
                            {{trans('messages.Quick links')}}
                        </h4>
                        <div class="footer-title-line"></div>
                        <ul class="footer-menu">
                            <li>
                                <a href="#">
                                    {{trans('messages.Special Offers')}}
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>                                    {{trans('messages.Last News')}}
                        </h4>
                        <div class="footer-title-line"></div>
                        <ul class="footer-blog">
                            <li>
                                <div class="col-md-3 col-sm-4 col-xs-4 blg-thumb p0">
                                    <a href="{{route('blog',['symbol' => $blogs->symbol])}}">
                                        <img style="border-radius: 0px;height: 46px;"
                                             src="{{url('storage/app/public/images/blog/'.$blogs->image)}}">
                                    </a>

                                </div>
                                <div class="col-md-8  col-sm-8 col-xs-8  blg-entry">
                                    <h6><a href="{{route('blog',['symbol' => $blogs->symbol])}}">
                                            {{$blogs->name}}</a></h6>
                                    <p></p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer news-letter">
                        <h4>
                            {{trans('messages.Stay in touch')}}
                        </h4>
                        <div class="footer-title-line"></div>

                        <div class="social text-center">
                            <ul>
                                @if (Config::get('site_settings.twitter') != '')
                                    <li>
                                        <a class="wow fadeInUp animated" href="{{Config::get('site_settings.twitter')}}"
                                           target="_blank">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                    </li>
                                @endif
                                @if (Config::get('site_settings.facebook') != '')
                                    <li>
                                        <a class="wow fadeInUp animated"
                                           href="{{Config::get('site_settings.facebook')}}"
                                           data-wow-delay="0.2s" target="_blank">
                                            <i class="fab fa-facebook"></i>
                                        </a>
                                    </li>
                                @endif
                                @if (Config::get('site_settings.youtube') != '')
                                    <li><a class="wow fadeInUp animated" href="{{Config::get('site_settings.youtube')}}"
                                           data-wow-delay="0.3s" target="_blank">
                                            <i class="fab fa-youtube"></i>
                                        </a>
                                    </li>
                                @endif
                                @if (Config::get('site_settings.instagram') != '')
                                    <li>
                                        <a class="wow fadeInUp animated"
                                           href="{{Config::get('site_settings.instagram')}}"
                                           data-wow-delay="0.4s" target="_blank">
                                            <i class="fab fa-instagram"></i>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 col-xs-12 wow fadeInRight animated text-center" style="padding: 0 !important;display: flex;align-items: center">
                    <img class="iata-footer-logo" width="100%"
                         src="{{ url('storage/app/public/images/settings/'.$IATA_Logo->value) }}"/>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-copy text-center">
        <div class="container">
            <div class="row">
                <div class="o-pull-left-en o-pull-right-ar">
<span style="color:white;">
{{trans('messages.All rights')}}</span>

                </div>
                <div class="bottom-menu o-pull-right-en o-pull-left-ar">
                    <ul>
                        <li>
                            <a class="wow fadeInUp animated" style="color:white;" href="{{route('policy')}}"
                               data-wow-delay="0.3s">
                                {{trans('messages.Privacy Policy')}}
                            </a>
                        </li>
                        <li>
                            <a class="wow fadeInUp animated" style="color:white;" href="{{route('terms')}}"
                               href="page.php?menu=21" data-wow-delay="0.6s">
                                {{trans('messages.Terms And Condition')}}
                            </a>

                        </li>
                        <li>
                            <a class="wow fadeInUp animated" style="color:white;" href="{{route('sitemap')}}"
                               data-wow-delay="0.6s">
                                {{trans('messages.Site Map')}}
                            </a>
                        </li>
                        <li>
                            <a class="wow fadeInUp animated" style="color:white;" href="{{route('support')}}"
                               data-wow-delay="0.6s">
                                {{trans('messages.Support')}}
                            </a>
                        </li>
                        <li>
                            <a class="wow fadeInUp animated" style="color:white;" href="{{route('about')}}"
                               data-wow-delay="0.2s">
                                {{trans('messages.About us')}}
                            </a>
                        </li>

                        <li>
                            <a class="wow fadeInUp animated" style="color:white;" href="{{route('contact')}}"
                               data-wow-delay="0.6s">
                                {{trans('messages.Contact Us')}}
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="whats-app-icon">
        <a href="https://api.whatsapp.com/send?phone={{$whatsapp->value}}" target="_blank">
            <i class="fab fa-whatsapp"></i>
        </a>
    </div>
    <div class="messenger-icon">
        <a href="https://m.me/1631151260479094" target="_blank">
            <i class="fab fa-facebook-messenger" aria-hidden="true"></i>
        </a>
    </div>
</div>
<div class="modal fade" id="myModalEnquiry" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
        </div>
    </div>
</div>
<div class="modal fade" id="ModalViewEnquiry" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
        </div>
    </div>
</div>
<div class="modal fade" id="myModalEnquiryHeader" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 style="margin: 10px 0;">
                    {{trans('messages.Enquiry_information')}} </h4>
            </div>
            <div class="modal-body">
                <h1 class="s-property-title">
                    {{trans('messages.Please_fill')}}
                </h1>
                <form method="POST" id="enquiry-data" action="{{route('send-enquiry-header')}}">
                    @csrf
                    <div class="row">
                        <div class="col-md-12 col-xs-12 p-b-15">
                            <input type="text" class="form-control" id="name" name="name"
                                   placeholder="{{trans('messages.Full_name')}}" required="true"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12 p-b-15">
                            <input type="text" class="form-control" id="phone" name="phone"
                                   placeholder="{{trans('messages.Mobile')}}" required="true"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12 p-b-15">
                            <input type="email" class="form-control" id="email" name="email"
                                   placeholder="{{trans('messages.Email')}}" required="true"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12 p-b-15">
                            <input type="text" class="form-control" id="address"
                                   placeholder="{{trans('messages.Address')}}" name="address"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12 p-b-15">
<textarea class="form-control" id="message" name="message" placeholder="{{trans('messages.Message')}}" rows="5"
          style="max-width: 100%"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <input type="submit" class="navbar-btn nav-button login" data-wow-delay="0.45s"
                                   value="{{trans('messages.Send')}}">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<div class="modal fade favorite-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>
<!-- Scripts -->
<script>
    var translations = {!! \Cache::get('translations') !!};
</script>
<script src="{{ asset('frontend/js/jquery-1.10.2.min.js') }}"></script>
<script src="{{ asset('frontend/js/modernizr-2.6.2.min.js') }}"></script>
{{--    <script src="{{ asset('frontend/js/query.creditCardValidator.js') }}" ></script>--}}
{{--    <script src="{{ asset('frontend/js/checkout.js.js') }}" ></script>--}}
<script src="{{ asset('frontend/'.app()->getLocale().'/bootstrap/js/bootstrap.js') }}"
></script>
<script src="{{ asset('frontend/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('frontend/js/bootstrap-hover-dropdown.js') }}"></script>


<script src="{{ asset('frontend/js/easypiechart.min.js') }}"></script>
<script src="{{ asset('frontend/js/jquery.easypiechart.min.js') }}"></script>

<script src="{{ asset('frontend/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('frontend/js/wow.js') }}"></script>

<script src="{{ asset('frontend/js/icheck.min.js') }}"></script>
<script src="{{ asset('frontend/js/price-range.js') }}"></script>
<script src="{{ asset('frontend/js/lightslider.min.js') }}"></script>

<script src="{{ asset('frontend/js/moment.js') }}"></script>
<script src="{{ asset('frontend/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('frontend/js/bootstrap-datepaginator.js') }}"></script>
<script src="{{ asset('frontend/js/slick.js') }}"></script>
<script src="{{ asset('frontend/js/swiper.js') }}"></script>
<script src="{{ asset('frontend/js/jquery.slidereveal.min.js') }}"></script>
<script src="{{ asset('frontend/js/sweetalert.min.js') }}"></script>

<script src="{{ asset('frontend/js/main.js') }}"></script>
<script>
    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function () {
        scrollFunction()
    };

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            $("#back-to-top-button").css('display', "block");
        } else {
            $("#back-to-top-button").css('display', "none");
        }
    }

    // When the user clicks on the button, scroll to the top of the document
    function shareFacebookButton() {
        $('#share-facebook').click();
    }

    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    (function ($) {


        /* ----------------------------------------------------------- */
        /*  1. SCROLL DOWN
        /* ----------------------------------------------------------- */


        $(".mu-scrolldown").click(function (event) {
            event.preventDefault();
            //calculate destination place
            var dest = 0;
            if ($(this.hash).offset().top > $(document).height() - $(window).height()) {
                dest = $(document).height() - $(window).height();
            } else {
                dest = $(this.hash).offset().top;
            }
            //go to destination
            $('html,body').animate({scrollTop: dest}, 1000, 'swing');
        });


        /* ----------------------------------------------------------- */
        /*  2. TRAVELERS TESTIMONIALS (SLICK SLIDER)
        /* ----------------------------------------------------------- */

        $('.mu-testimonial-slide').slick({
            arrows: false,
            dots: true,
            infinite: true,
            speed: 500,
            autoplay: true,
            cssEase: 'linear'
        });


        /* ----------------------------------------------------------- */
        /*  3. SCROLL TOP BUTTON
        /* ----------------------------------------------------------- */

        //Check to see if the window is top if not then display button

        jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > 300) {
                jQuery('.scrollToTop').fadeIn();
            } else {
                jQuery('.scrollToTop').fadeOut();
            }
        });

        //Click event to scroll to top

        jQuery('.scrollToTop').click(function () {
            jQuery('html, body').animate({scrollTop: 0}, 800);
            return false;
        });


        /* ----------------------------------------------------------- */
        /*  4. CLIENTS SLIDEER ( SLICK SLIDER )
        /* ----------------------------------------------------------- */

        $('.mu-clients-slider').slick({
            slidesToShow: 5,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: true,
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: true,
                        slidesToShow: 2
                    }
                }
            ]
        });


    })(jQuery);


</script>
</body>
</html>
