<ul class="nav nav-tabs pl15 pt10" style="padding:0px">
    <li class="active">
        <a class="hidden-xs" href="#tab1primary" data-toggle="tab">
            <i class="fas fa-file-alt"></i>
            <br> {{trans('messages.Overview')}}
        </a>
        <a class="hidden-lg hidden-md hidden-sm" href="#tab1primary" data-toggle="tab">
            <i class="fas fa-file-alt"></i>
            <br> {{trans('messages.Overview_mob')}}
        </a>
    </li>
    @if ($package->inclusions || $package->exclusions)
        <li class="">
            <a href="#tab3primary" data-toggle="tab">
                <i class="fas fa-th-list">
                </i>
                <br>
                {{trans('messages.Inclusion')}}
            </a>
        </li>
    @endif
    @if ($hotel)
        <li class="">
            <a href="#tab4primary" data-toggle="tab">
                <i class="fas fa-bed" aria-hidden="true"></i>
                <br>
                {{trans('messages.Hotel')}}
            </a>
        </li>
    @endif
    @if($package->flights)
        <li class="">
            <a href="#tab5primary" data-toggle="tab">
                <i class="fas fa-fighter-jet"></i>
                <br>
                {{trans('messages.Flight')}}
            </a>
        </li>
    @endif
    @if($package->days)
        <li class="">
            <a href="#tab6primary" data-toggle="tab">
                <i class="fas fa-suitcase">
                </i>
                <br>
                {{trans('messages.Itinerary2')}}
            </a>
        </li>
    @endif
    @if ($package->terms_condition != '' || $package->cancellation_policy !=
    '')
        <li class="">
            <a class="hidden-xs" href="#tab7primary" data-toggle="tab">
                <i class="fas fa-pen-nib">
                </i>
                <br>
                {{trans('messages.Terms_and_Condition')}}
            </a>
            <a class="hidden-lg hidden-md hidden-sm" href="#tab7primary"
               data-toggle="tab">
                <i class="fas fa-pen-nib">
                </i>
                <br>
                {{trans('messages.Terms_and_Condition_mob')}}
            </a>
        </li>
    @endif
</ul>
