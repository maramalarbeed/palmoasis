<div class="tab-pane fade active in" id="tab1primary">
    <div class="section">
        <h1 class="s-property-title">
            {{trans('messages.Overview')}}
        </h1>

        <div class="s-property-content" style="margin-top: -15px;">
            <p>
                {!! $package->overview !!}
            </p>

        </div>
    </div>
    <div class="section" style="margin-top:-28px">
        @include('frontend.blocks.in_out')
    </div>

    @if ($hotel)
        <div class="section">
            <div class="row">
                <h1 class="s-property-title margin-title">
                    {{trans('messages.Hotel_Details')}}
                </h1>
                @include('frontend.blocks.hotel')
            </div>
        </div>
    @endif
    @if ($package->flights)
        <div class="section">
            <h4 class="s-property-title">
                {{trans('messages.Flight_Details')}}
            </h4>
            @include('frontend.blocks.flight')

        </div>
    @endif
    @if ($package->transfers)
        <div class="section">
            <h4 class="s-property-title">
                {{trans('messages.Transfer_Details')}}
            </h4>
            @include('frontend.blocks.transfer')
        </div>
    @endif
    @if ($package->days)
        <div class="section">
            <div class="row">
                <h4 class="s-property-title margin-title">
                    {{trans('messages.Your_Holiday_itinaraly')}}
                </h4>
                <div class="day-timeline">
                    @include('frontend.blocks.day')
                </div>
            </div>
        </div>
    @endif
    @if($package->additional_info != '')
        @if(isset($package->open_additional_info) && $package->open_additional_info == '1')
            <div class="section">
                <h1 class="s-property-title slide" data-toggle="collapse"
                    data-target="#additional-info">
                    <i class="fas fa-minus first"></i>
                    {{trans('messages.Additional_information')}}
                </h1>
                <div
                    class="s-property-content panel-collapse fqa-body"
                    id="additional-info">
                    <p>
                        {!! $package->additional_info !!}
                    </p>
                </div>
            </div>
        @else
            <div class="section">
                <h1 class="s-property-title slide" data-toggle="collapse"
                    data-target="#additional-info">
                    <i class="fas fa-plus"></i>
                    {{trans('messages.Additional_information')}}
                </h1>
                <div
                    class="s-property-content panel-collapse collapse fqa-body"
                    id="additional-info">
                    <p>
                        {!! $package->additional_info !!}
                    </p>
                </div>
            </div>
        @endif

    @endif
    @if($package->packageCountry)
        <div class="section">
            <h4 class="s-property-title">
                {{trans('messages.Information_Country')}}
            </h4>
            <p>
                <b>
                    {{trans('messages.Country_Name')}} :</b>
                {{$package->packageCountry->name}}</br>
                <b>{{trans('messages.flag')}} :</b>
                <img style="width:50px;"
                     src="{{url('storage/app/public/images/country/' . $package->packageCountry->flag)}}"/></br>
                <b>{{trans('messages.Capital')}} :</b>
                {{$package->packageCountry->capital}}</br>
                <b>{{trans('messages.Currency')}} :</b>
                {{$package->packageCountry->currency}}</br>
                <b>{{trans('messages.Convert_Currency')}} :</b>
                {{$package->packageCountry->convert_currency}}</br>
                <b>{{trans('messages.Official_language')}} :</b>
                {{$package->packageCountry->official_lang}}</br>
            </p>
        </div>
        @if ($package->packageCountry->visa_info != '')
            <div class="section">
                <h1 class="s-property-title slide" data-toggle="collapse"
                    data-target="#country-visa-info">
                    <i class="fas fa-plus"></i>
                    {{trans('messages.Visa_Information')}}
                </h1>

                <div class="s-property-content panel-collapse collapse fqa-body"
                     id="country-visa-info">
                    <p>
                        {!! $package->packageCountry->visa_info !!}
                    </p>
                </div>
            </div>
        @endif
    @endif
    @if ($package->terms_condition != '')
        <div class="section">
            @if(isset($package->open_term) && $package->open_term == '1')
                <h1 class="s-property-title slide" data-toggle="collapse"
                    data-target="#terms-condition-2">
                    <i class="fas fa-minus first"></i>
                    {{trans('messages.Terms_and_Condition')}}
                </h1>
                <div
                    class="s-property-content panel-collapse fqa-body"
                    id="terms-condition-2">
                    <p>
                        {!! $package->terms_condition !!}
                    </p>
                </div>
            @else
                <h1 class="s-property-title slide" data-toggle="collapse"
                    data-target="#terms-condition-2">
                    <i class="fas fa-plus"></i>
                    {{trans('messages.Terms_and_Condition')}}
                </h1>
                <div
                    class="s-property-content panel-collapse collapse fqa-body"
                    id="terms-condition-2">
                    <p>
                        {!! $package->terms_condition !!}
                    </p>
                </div>
            @endif
        </div>
    @endif
    @if ($package->cancellation_policy != '')
        <div class="section">
            @if(isset($package->open_cancellation) && $package->open_cancellation == '1')
                <h1 class="s-property-title slide" data-toggle="collapse"
                    data-target="#cancellation-policy-2">
                    <i class="fas fa-minus first"></i>
                    {{trans('messages.Cancellation_Policy')}}
                </h1>
                <div
                    class="s-property-content panel-collapse fqa-body"
                    id="cancellation-policy-2">
                    <p>
                        {!! $package->cancellation_policy !!}
                    </p>
                </div>
            @else
                <h1 class="s-property-title slide" data-toggle="collapse"
                    data-target="#cancellation-policy-2">
                    <i class="fas fa-plus"></i>
                    {{trans('messages.Cancellation_Policy')}}
                </h1>
                <div
                    class="s-property-content panel-collapse collapse fqa-body"
                    id="cancellation-policy-2">
                    <p>
                        {!! $package->cancellation_policy !!}
                    </p>
                </div>
            @endif
        </div>
    @endif
</div>
@if ($hotel)
    <div class="tab-pane fade" id="tab4primary">
        <div class="acc-body">
            @include('frontend.blocks.hotel')
        </div>
    </div>
@endif
@if ($package->days)
    <div class="tab-pane fade" id="tab6primary">
        <div class="section">
            <h4 class="s-property-title">
                {{trans('messages.Your_Holiday_itinaraly')}}
            </h4>
            <div class="day-timeline">
                @include('frontend.blocks.day')
            </div>
        </div>

    </div>
@endif
@if (($package->terms_condition != '' ) && $package->cancellation_policy
!= '')
    <div class="tab-pane fade" id="tab7primary">
        <div class="acc-body">
            @if($package->terms_condition != '' )
                <div class="section">
                    @if(isset($package->open_term) && $package->open_term == '1')
                        <h1 class="s-property-title slide" data-toggle="collapse"
                            data-target="#terms-condition-3">
                            <i class="fas fa-minus first"></i>
                            {{trans('messages.Terms_and_Condition')}}
                        </h1>
                        <div
                            class="s-property-content panel-collapse fqa-body"
                            id="terms-condition-3">
                            <p>
                                {!! $package->terms_condition !!}
                            </p>
                        </div>
                    @else
                        <h1 class="s-property-title slide" data-toggle="collapse"
                            data-target="#terms-condition-3">
                            <i class="fas fa-plus"></i>
                            {{trans('messages.Terms_and_Condition')}}
                        </h1>
                        <div
                            class="s-property-content panel-collapse collapse fqa-body"
                            id="terms-condition-3">
                            <p>
                                {!! $package->terms_condition !!}
                            </p>
                        </div>
                    @endif
                </div>
            @endif
            @if($package->cancellation_policy != '')
                <div class="section">
                    @if(isset($package->open_cancellation) && $package->open_cancellation == '1')
                        <h1 class="s-property-title slide" data-toggle="collapse"
                            data-target="#cancellation-policy-3">
                            <i class="fas fa-minus first"></i>
                            {{trans('messages.Cancellation_Policy')}}
                        </h1>
                        <div
                            class="s-property-content panel-collapse fqa-body"
                            id="cancellation-policy-3">
                            <p>
                                {!! $package->cancellation_policy !!}
                            </p>
                        </div>
                    @else
                        <h1 class="s-property-title slide" data-toggle="collapse"
                            data-target="#cancellation-policy-3">
                            <i class="fas fa-plus"></i>
                            {{trans('messages.Cancellation_Policy')}}
                        </h1>
                        <div
                            class="s-property-content panel-collapse collapse fqa-body"
                            id="cancellation-policy-3">
                            <p>
                                {!! $package->cancellation_policy !!}
                            </p>
                        </div>
                    @endif
                </div>
            @endif
        </div>
    </div>
@endif
@if ($package->flights)
    <div class="tab-pane fade" id="tab5primary">
        <div class="panel-body recent-property-widget">
            <h4 class="s-property-title">
                {{trans('messages.Flight_Details')}}
            </h4>
            @include('frontend.blocks.flight')
        </div>

    </div>

@endif
@if ($package->inclusions || $package->exculusions)
    <div class="tab-pane fade" id="tab3primary">
        <div class="section">
            @include('frontend.blocks.in_out')
        </div>
    </div>
@endif
