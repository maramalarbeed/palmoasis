@extends('backend.layouts.app')

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit Homepage Images </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active">Edit Img</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="card card-primary card-outline card-outline-tabs">
                        @if($errors->count() != 0)
                            <div class="form-group">
                                <div class="col-8 col-md-8">
                                    @foreach ($errors->all() as $error)
                                        <p>{{$error}}</p>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <form id="demo-form2" method="POST" action="{{ route('admin.home.info.save') }}"
                      enctype="multipart/form-data" style="width: 100%">
                    @csrf
                    @method('patch')
                    <div class="col-12 col-sm-12">
                        <div class="card card-primary card-outline card-outline-tabs">
                            @foreach($reasons as $reason)
                                <div class="row" style="padding: 15px">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>En text</label>
                                            <input type="text" class="form-control"
                                                   name="reason[{{$reason->id}}][intro_en]"
                                                   value="{{$reason->intro_en}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Ar text</label>
                                            <input type="text" class="form-control"
                                                   name="reason[{{$reason->id}}][intro_ar]"
                                                   value="{{$reason->intro_ar}}">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <!-- Image -->
                                        <div class="form-group">
                                            <label for="image">Image</label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input"
                                                           id="image" name="reason[{{$reason->id}}][image]">
                                                    <label class="custom-file-label" for="image">Choose
                                                        file</label>
                                                </div>
                                            </div>
                                            @if(isset($reason->header_image_en))
                                                <img style="width: 100px;"
                                                     src="{{ url('storage/app/public/images/info/'.$reason->header_image_en) }}"/>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-12 col-sm-12">
                        <div class="card card-primary card-outline card-outline-tabs">
                            <div class="col-md-12" style="padding: 15px">
                                @if($service_image->is_image == 1)
                                    <div class="form-group">
                                        <div class="icheck-primary d-inline">
                                            <input type="radio" id="radioPrimary1" checked name="is_image" value="1"
                                                   onclick="showImage()">
                                            <label for="radioPrimary1">
                                                Image
                                            </label>
                                        </div>
                                        <div class="icheck-primary d-inline">
                                            <input type="radio" id="radioPrimary2" name="is_image" value="0"
                                                   onclick="showVideo()">
                                            <label for="radioPrimary2">
                                                Video
                                            </label>
                                        </div>
                                    </div>
                                @else
                                    <div class="form-group">
                                        <div class="icheck-primary d-inline">
                                            <input type="radio" id="radioPrimary1" name="is_image" value="1"
                                                   onclick="showImage()">
                                            <label for="radioPrimary1">
                                                Image
                                            </label>
                                        </div>
                                        <div class="icheck-primary d-inline">
                                            <input type="radio" id="radioPrimary2" checked name="is_image" value="0"
                                                   onclick="showVideo()">
                                            <label for="radioPrimary2">
                                                Video
                                            </label>
                                        </div>
                                    </div>
                            @endif
                            @php
                                if($service_image->is_image == 1){
    $image = 'display: block' ;
    $video = 'display: none';
}
else{
       $image = 'display: none' ;
    $video = 'display: block';
}
                            @endphp
                            <!-- Image -->
                                <div class="form-group" id="image-div" style="{{$image}}">
                                    <label for="service_image">Service Image</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input"
                                                   id="service_image" name="service_image">
                                            <label class="custom-file-label" for="service_image">Choose
                                                file</label>
                                        </div>
                                    </div>
                                    @if(isset($service_image->header_image_en))
                                        <img style="width: 100px;"
                                             src="{{ url('storage/app/public/images/info/'.$service_image->header_image_en) }}"/>
                                    @endif
                                </div>
                                <!-- Video -->
                                <div class="form-group" id="video-div" style="{{$video}}">
                                    <label for="service_image">Service Video</label>
                                    <input type="text" name="service_video" value="{{$service_image->header_image_en}}"
                                           class="form-control">
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12">
                        <div class="card card-primary card-outline card-outline-tabs">
                            <div class="col-md-12">
                                <label for="footer_image">Footer Text</label>
                                <div class="form-group">

                                    <div class="col-md-6" style="float: left">
                                        <label>En text</label>
                                        <input type="text" class="form-control"
                                               name="footer_text_en"
                                               value="{{$footer_image->intro_en}}">
                                    </div>

                                    <div class="col-md-6" style="float: left">
                                        <label>Ar text</label>
                                        <input type="text" class="form-control"
                                               name="footer_text_ar"
                                               value="{{$footer_image->intro_ar}}">
                                    </div>
                                </div>

                                <!-- Image -->
                                <div class="form-group">
                                    <label for="footer_image">Footer Image</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input"
                                                   id="footer_image" name="footer_image">
                                            <label class="custom-file-label" for="footer_image">Choose
                                                file</label>
                                        </div>
                                    </div>
                                    @if(isset($footer_image->header_image_en))
                                        <img style="width: 100px;"
                                             src="{{ url('storage/app/public/images/info/'.$footer_image->header_image_en) }}"/>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12">
                        <div class="card card-primary card-outline card-outline-tabs">
                            <div class="form-group" style="padding: 15px;">
                                <input type="submit" class="btn btn-success" value="Save"/>
                            </div>
                        </div>
                    </div>

                </form>

            </div>

        </div>
    </section>


@endsection
