@extends('backend.layouts.app')

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="float-left col-sm-6">
                    <h1>Add Tour </h1>
                </div>
                <div class="float-left col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item"><a
                                href="{{route('admin.tours.index',['country' => $country->id])}}">Tours of
                                ({{$country->name_en}})
                            </a></li>
                        <li class="breadcrumb-item active">Add Tour</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@endsection
