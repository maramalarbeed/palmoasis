<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>

<body>
<style>
    .btn-default {
        border-radius: 1px;
        padding: 10px 20px;
        border: 1px solid #38b54a;
        border-top-color: rgb(56, 181, 74);
        border-right-color: rgb(56, 181, 74);
        border-bottom-color: rgb(56, 181, 74);
        border-left-color: rgb(56, 181, 74);
        color: #000;
        background-color: #38b54a;
        border-color: #F0F0F0;
        font-weight: 600;
        text-decoration: none;
    }
</style>
<div class="col-md-12 col-xs-12 col-sm-12">

    <h2> Reset Password Email From Palmoasisholidays Website </h2>
    <h3>Dear {{$user->name}} please follow the reset password link </h3>
    <div class="form-group">
        <a href="{{route('member.password.reset', ['token' ,$token])}}" class="btn-default"> Reset password </a>
    </div>
</div>
</body>
</html>
